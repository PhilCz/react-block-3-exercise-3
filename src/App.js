import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Email from "./components/email";
import Password from "./components/password";
import Submit from "./components/submit";

class App extends React.Component {
  state = {
    email: "",
    password: "",
  };
  handleInput = (event) => {
    let name = event.target.name;
    let value = event.target.value;
    this.setState({ [name]: value });
  };

  getDataOnClick = () => {
    alert(
      `Your email is ${this.state.email} and your password is ${this.state.password}`
    );
  };
  render() {
    return (
      <div className="App">
        <Email getEmail={this.handleInput} />
        <Password getPassword={this.handleInput} />
        <Submit
          submit={this.getDataOnClick}
          data={{ email: this.state.email, password: this.state.password }}
        />
      </div>
    );
  }
}
export default App;
